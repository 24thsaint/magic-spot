const expect = require('chai').expect
const mongoose = require('mongoose')

let mongoURI = ""

switch(process.env.NODE_ENV) {
  case 'production' :
  mongoURI = "mongodb://mongo/magic-prod"
  break
  case 'ci' :
  mongoURI = "mongodb://mongo/magic-test"
  break
  default:
  mongoURI = "mongodb://localhost:27017/magic"
}

const db = mongoose.connect(mongoURI)
mongoose.Promise = Promise

describe('Check contents of database', function() {

  const personSchema = mongoose.Schema({
    name: String
  })
  const Person = mongoose.model('Person', personSchema)
  const teacher = new Person({ name: 'Richard' })

  beforeEach((done) => {
    Person.remove({})
      .then(() => {
        done()
      })
  })

  it('Should be empty', function(done) {
    console.log('plss workkk')

    teacher.save()
    .then((result) => {
      return Person.find().exec()
    })
    .then((result2) => {
      expect(result2).to.have.length(1)
      done()
    }).catch((error) => {
      done(error)
    })
  })
})
